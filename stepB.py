#databricks syntax, needs to be altered depending on later usage


#---------------------------general loading and removing header----------------
#load csv file (creates rdd) 
#data=sc.textFile("/FileStore/tables/check.csv")
data=sc.textFile("/FileStore/tables/biggest_data_check-46637.csv")
#pipeline rdd containing each original excel row seperatly  
splitedRdd=data.map(lambda x: (x.split(',')))

#first line (column headers)
header=splitedRdd.first()

#remove said header, still in a pipeline rdd format
#each element in the rdd: (irrelevant column, reviewerID, examineeID, tounge, content, tooShort, didAnswer, P/F, date, time)
removedHeaderRdd=splitedRdd.filter(lambda row:row != header)#.collect()#.toDF()
#---------------------------end of general-------------------------------------

#creates rdd with only 2 relevant columns (reviewerID, essayID), without duplicates
distinctLinesRdd=removedHeaderRdd.map(lambda row: (row[1],row[3])).distinct().cache()#.toDF()

#reduces all key related values, reviewerID being the key, essayID being the value: (reviewerID,[essayID1, essayID2, ...])
reducedByKeyRdd=distinctLinesRdd.map(lambda x: (x[0], [x[1]])).reduceByKey(lambda row1,row2: row1+row2)#.toDF()#.collect()

#output rdd, consisting of rows: (reviewerID,[essayID1, essayID2, ...], numOfEssays)
outputRdd=reducedByKeyRdd.map(lambda x: (x[0],x[1],len(x[1]))).toDF(['reviewerID','essayIDs','numOfEssays' ])#.collect()

#sort the table by 'reviewerID' column in ascending order
finalSorted=outputRdd.sort(['reviewerID'], ascending=True)
display(finalSorted)

#outputRdd.show()