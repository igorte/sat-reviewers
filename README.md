
## About me
My name is Igor Tenenboim.  
I am a 4th year student of software engeneering At the Azrieli Academic College of Engineering.  
This repository represents my graduate work.

---
## Side Note
The project was created in cooperation with the Israeli National Institute for Testing and Evaluation (NITE) as part of the final project in the Department of Software Engineering
At the Azrieli Academic College of Engineering.

*Being that the project is based on **real** data provided by the MOE, the data itself will not be uploaded to this repository*

---
## The project
*This is a research project.*

The purpose of the project is to reduce marketplace response time by scoring workers.  
The data we are given by NITE is in large EXCEL files. Those files are our raw data, containing essay IDs, reviewer IDs for said essays, work time (dates and time of connecting/disconnecting to/from the system) and different scoring aspects.  
In order to reach the project's purpose, our main goal is to find the right correlation between the given parameters in order to score the reviewers properley, thus reaching further conclusions on proper and efficient task allocations (reducing the overall response time).  

After thought with the project's guide, it was decided We will implement the needed calculation and algorithms using Apache Spark over Python (AKA py-spark), which is a parallel computation tool for large scale data-sets. The code is tested in DataBricks environment.
Also, after the data manipulation is completed, a simulation is generated based on said data as input (using AnyLogic simulator) that will provide further statistics. 

---
## Explanation on the files
The input for all the files is the same (data recieved from NITE).

* stepA.py - calculates the worktime of each reviewer and the eesay he/she checked. Output is a table fo rows of type [Reviewer ID, Essay ID, Work time in seonds].

* stepB.py - calculates the number of essays each reviewer checked, and prints their ID's. Output is a table fo rows of type [Reviewer ID, Essay IDs array, Number of essays].

* stepC.py - calculates the number of reviewers that each essay was checked by, and prints their ID's. Output is a table of type [Essay ID, Reviewer IDs array, Number of reviewers].

* SAT_reviewers.py - The main program. Uses the above 3 programs and more, to manipulate the data to apply K-means clustering algorithm for further indication on how many gruops of reviewers should be divided to. Output is the prediction of how many groups of similar reviewers there are, and to which each of them belongs to.

* SAT_reviewers_simulation folder - contains the AnyLogic simulation created for statistical analisys. 

---
## How to run

1. On Your computer  

	- create csv file in your computer  
	- copy content of EXEL file (xlsx) to said csv file and save  


2. In Databricks  

	- create new notebook   
	- copy code from my project into the notebook  
	- upload csv file to table data  
	- replace path with your path to uploaded csv in code  
	- create cluster and attach project to it  
	- press run  
*output should be a table with wanted data at the bottom of the page*
 
 
3. In AnyLogic

	- download AnyLogic software from the official website link (given below)
	- download the simulation folder from my repository 
	- click the alf file twice
	- the project should open at this point, simply click the green "play" button to run the simulation

---
## Helpful links
Apache Spark - [Official website](https://spark.apache.org/), [Wikipedia](https://en.wikipedia.org/wiki/Apache_Spark)  
AnyLogic simulator - [Official website](https://www.anylogic.com/company/about-us/), [Wikipedia](https://en.wikipedia.org/wiki/AnyLogic)  
DataBricks - [Official website](https://databricks.com/), [Wikipedia](https://en.wikipedia.org/wiki/Databricks) 


