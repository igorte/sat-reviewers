#databricks syntax, needs to be altered depending on later usage
#needs to take excel input and convert to csv

from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.linalg import Vectors
#---------------------------general loading and removing header----------------
#load csv file (creates rdd) 
data=sc.textFile("/FileStore/tables/biggest_data_check-46637.csv")
#pipeline rdd containing each original excel row seperatly  
splitedRdd=data.map(lambda x: (x.split(',')))

#first line (column headers)
header=splitedRdd.first()

#remove said header, still in a pipeline rdd format
#each element in the rdd: (irrelevant column, reviewerID, examineeID, tounge, content, tooShort, didAnswer, P/F, date, time)
removedHeaderRdd=splitedRdd.filter(lambda row:row != header)#.toDF()
#---------------------------end of general-------------------------------------

#returns only the relevant cols in the data:
#((reviwerId, essayId), [(P/F, date, time)])
def cleanCols(row):
  return [(row[1], row[3]), [(row[8], row[9], row[10])]]

#built-in pyspark time and date difference calculator
def time_delta(x,y): 
    from datetime import datetime
    start = datetime.strptime(x, '%d/%m/%Y, %H:%M:%S')
    end = datetime.strptime(y, '%d/%m/%Y, %H:%M:%S')
    delta = (end-start).total_seconds()
    return delta

#for every key (reviwerId, essayId) checks wich one is F and P in order to subtract: (finish time - start time)   
def beginingOrEnd(row):
  length= len(row[1]) #if there is less then 2 elements it means there is a problem (no beggining/end)
  if length<2:
    diff=1
  else:
    temp1, temp2= row[1][length-2][0], row[1][length-1][0] #P or F
    dateAndTime2=row[1][length-1][1]+', '+row[1][length-1][2] #conctinate date and time in to the format: ('dd/mm/Year, H:M:S')
    dateAndTime1=row[1][length-2][1]+', '+row[1][length-2][2]
    if temp1=='P' and temp2=='F':
      diff=time_delta(dateAndTime1,dateAndTime2)
    elif temp1=='F' and temp2=='P':
      diff=time_delta(dateAndTime2,dateAndTime1)
    else: 
      diff=1
  #return (reviewerID, essayID, total work time in seconds)  
  return [row[0][0],row[0][1], diff]

def denseVector(row):
  return row+[Vectors.dense(row[2])]

#groups relevant columns by key (reviewerID, essayID) and value (P/F, date, time)
#then reduces values by said key into: (reviewerID, essayID), [array of elemnts : (P/F, date, time)]
reducedDataRdd=removedHeaderRdd.map(lambda row: cleanCols(row)).reduceByKey(lambda x,y: x+y)#.toDF()


#calculate the time difference, then creates another column that contains the seconds param in the form of vector for further k means implemantation
#returns final df containing elements of type (reviewerID, essayID, total work time in seconds)
finalWorkTime=reducedDataRdd.map(lambda row: beginingOrEnd(row)).map(lambda row: denseVector(row)).toDF(["reviewerID", "essayID", "time in seconds","time(vector)"])#time in seconds(ERROR => -1)

#sort the table by 'reviewerID' column in ascending order 
finalSortedTime=finalWorkTime.sort(['reviewerID'], ascending=True)

#display(finalSortedTime)

#vector assembler assembles the time vector from the dense vector for each row for later k means implementation
#otput column is called "features"
vecAssembler = VectorAssembler(inputCols=["time(vector)"], outputCol="features")
#create new df containing the features column as a new column in regards to the previous df
new_df = vecAssembler.transform(finalSortedTime)
#display(new_df)
#choose number of clusters and seeds for the k means function
kmeans = KMeans(k=4, seed=1)  # 4 clusters here
#the result
model = kmeans.fit(new_df.select("features")) 
#create new df containing the clustering prediction column as a new column in regards to the previous df
transformed = model.transform(new_df)
#display(transformed)

#deletes the unnecessary columns 
df2=transformed.drop('features', 'time(vector)')
display(df2)

# #final step required: figure out where to write the data (csv file that sits...?)