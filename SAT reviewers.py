#---------------------------------------------------------IMPORTS---------------------------------------------------
from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.linalg import Vectors
#-------------------------------------------------------END OF IMPORTS----------------------------------------------


#----------------------------------------------------FUNCTIONS FOR STEP A-------------------------------------------

#returns only the relevant cols in the data:
#((reviwerId, essayId), [(P/F, date, time)])
def cleanCols(row):
  return [(row[1], row[3]), [(row[8], row[9], row[10])]]

#built-in pyspark time and date difference calculator
def time_delta(x,y): 
    from datetime import datetime
    start = datetime.strptime(x, '%d/%m/%Y, %H:%M:%S')
    end = datetime.strptime(y, '%d/%m/%Y, %H:%M:%S')
    delta = (end-start).total_seconds()
    return delta

#for every key (reviwerId, essayId) checks wich one is F and P in order to subtract: (finish time - start time)   
def beginingOrEnd(row):
  #if there is less then 2 elements it means there is a problem (no beggining/end)
  length= len(row[1]) 
  if length<2:
    diff=1
  else:
    temp1, temp2= row[1][length-2][0], row[1][length-1][0] #P or F
    #concatinate date and time in to the format: ('dd/mm/Year, H:M:S')
    dateAndTime2=row[1][length-1][1]+', '+row[1][length-1][2]
    dateAndTime1=row[1][length-2][1]+', '+row[1][length-2][2]
    if temp1=='P' and temp2=='F':
      diff=time_delta(dateAndTime1,dateAndTime2)
    elif temp1=='F' and temp2=='P':
      diff=time_delta(dateAndTime2,dateAndTime1)
    else: 
      diff=1
      
  #return (reviewerID, essayID, total work time in seconds)  
  return [row[0][0],row[0][1], diff]

#deletes all rows of reviewers with irregular number of essays checked (in our case, less than 250)
def deleteIrregulars(row):
  if row[2]==250:
    return row[0],row[1],row[2]
  
#------------------------------------------------END OF FUNCTIONS FOR STEP A------------------------------------------


#-----------------------------------------Step A(reviewer id, essay id, time in sec)----------------------------------
def stepA(removedHeaderRdd):
  #groups relevant columns by key (reviewerID, essayID) and value (P/F, date, time)
  #then reduces values by said key into: (reviewerID, essayID), [array of elemnts : (P/F, date, time)]
  reducedDataRdd=removedHeaderRdd.map(lambda row: cleanCols(row)).reduceByKey(lambda x,y: x+y)#.toDF()

  #seperated contains rdd with rows of kind (reviewerID, essayID, time)
  seperated=reducedDataRdd.map(lambda row: beginingOrEnd(row))
  
  #Delete irregulars by ID
  tempseperated=seperated.filter(lambda x: x[0] != '104').filter(lambda x: x[0] != '6048').filter(lambda x: x[0] != '6131').filter(lambda x: x[0] != '6134').filter(lambda x: x[0] != '6535')#.toDF(["reviewerID", "essayID","worktime"])

  #----------------------------------------------------------Delete Irregulars-------------------------------------------
  #temprdd contains rdd with rows of kind (reviewerID, essayIDs, amount of essays)
  temprdd=seperated.map(lambda x: (x[0], [x[2]])).reduceByKey(lambda row1,row2: row1+row2).map(lambda x: (x[0],x[1],len(x[1])))
  irregDeleted=temprdd.filter(lambda x: x[2] == 250).toDF(["reviewerID", "timeVector","length"])
  withoutIrreg=irregDeleted.sort(['reviewerID'], ascending=True)
  #----------------------------------------------------------End Of Delition---------------------------------------------
  
  
  
  
#------------------------------------------------Calculation of k means on reviewers' work time--------------------------
  #calculate the time difference, then creates another column that contains the seconds param in the form of vector for further k means implemantation
  #returns final df containing elements of type (reviewerID, essayID, total work time in seconds)
  finalWorkTime=seperated.map(lambda row: denseVector(row)).toDF(["reviewerID", "essayID", "time in seconds","time(vector)"])#time in seconds(ERROR => -1)

  #sort the table by 'reviewerID' column in ascending order 
  finalSortedTime=finalWorkTime.sort(['reviewerID'], ascending=True)

  #display(finalSortedTime)

  #vector assembler assembles the time vector from the dense vector for each row for later k means implementation
  #otput column is called "features"
  vecAssembler = VectorAssembler(inputCols=["time(vector)"], outputCol="features")
  
  #create new df containing the features column as a new column in regards to the previous df
  new_df = vecAssembler.transform(finalSortedTime)
  #display(new_df)
  
  #choose number of clusters and seeds for the k means function
  kmeans = KMeans(k=4, seed=1)  # 4 clusters here
  
  #the result
  #k means prediction based on every essay's review time
  model = kmeans.fit(new_df.select("features")) 
  
  #create new df containing the clustering prediction column as a new column in regards to the previous df
  transformed = model.transform(new_df)
  #display(transformed)

  #deletes the unnecessary columns 
  df2=transformed.drop('features', 'time(vector)')
#------------------------------------------------------------End of Calculation------------------------------------------
  return withoutIrreg, tempseperated 

#------------------------------------------------------------END OF STEP A-----------------------------------------------





#--------------------------------------------------GENERAL LOADING AND REMOVING HEADER-----------------------------------
#load csv file (creates rdd) 
data=sc.textFile("/FileStore/tables/biggest_data_check-46637.csv")

#pipeline rdd containing each original excel row seperatly  
splitedRdd=data.map(lambda x: (x.split(',')))

#first line (column headers)
header=splitedRdd.first()

#remove said header, still in a pipeline rdd format
#each element in the rdd: (irrelevant column, reviewerID, examineeID, tounge, content, tooShort, didAnswer, P/F, date, time)
removedHeaderRdd=splitedRdd.filter(lambda row:row != header)#.toDF()
#--------------------------------------------------------END OF GENERAL--------------------------------------------------


#turn each element (essId, time) from str to int/float accordingly
def strToFloat(row):
  for i in row[1]:
    i[0]=int(i[0])
    i[1]=float(i[1])
  return row

#find mean worktime for every essay
def mean(row):
  counter=0
  totalSum=0
  for i in row[1]:
    totalSum+=i[1]
    counter+=1
  #avg calculation
  mean=totalSum/counter
  return [row[0], row[1], mean]

import math
#find standard deviation for each essay's work time by different reviewers 
def std(row):
  counter=0
  mean=row[2]
  tempSum=0
  for i in row[1]:
    diff=i[1]-mean
    tempSum+=diff**2
    counter+=1
  res=tempSum/counter
  std=math.sqrt(res)
  return [row[0], row[1], row[2], std]

#find zTime (worktime-avg)/std
def zTime(row):
  avg= row[2]
  std=row[3]
  #create list to be filled with z time
  tempList=[]
  for i in row[1]:
    #calculate z time
    z=(i[1]-avg)/std
    #fill list with said calculation by appending in a loop
    tempList.append([i[0],z])
  return [row[0],row[1],row[2],row[3],tempList]

#sorts by reviewerId in the [reviewerId,zTime] column
def sortList(row):
  row[4].sort(key=lambda x: x[0])
  return row

#apply step A function
df1, temp=stepA(removedHeaderRdd)
#----------------------------------------------------------Save directly to file storage--------------------------------
#temp.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save("dbfs:/FileStore/df/Sample.csv")
#dbutils.fs.rm("/FileStore/df",True)
#----------------------------------------------------------saving option should remain under comment --------------------



#----------------------------------------------------------calculation of avg, std, ztime--------------------------------
#obtain key-value: [essayId,(reviewerId,Time)]
c= temp.map(lambda x: (x[1],[x[0],x[2]]))#.collect()#.toDF()

#manipulate rdd to turn second element to list, then cast float for average  and standard deviation calculation
d=c.groupByKey().map(lambda x : (x[0], list(x[1]))).map(lambda x: strToFloat(x)).map(lambda x: mean(x)).map(lambda x: std(x))

#calculate z time and sort by first element
e=d.map(lambda x: zTime(x)).sortBy(lambda a: a[0]).map(lambda x: sortList(x))#.toDF(["essayId","[reviewerId, workTime]","AVG","STD","[reviewerId, zTime]"])

#same as above, for later calculation of different variables
essRevTimeAvgStdZ=d.map(lambda x: zTime(x)).sortBy(lambda a: a[0]).map(lambda x: sortList(x)).toDF(["essayId","[reviewerId, workTime]","AVG","STD","[reviewerId, zTime]"])
#e=d.sortBy(lambda a: a[0]).toDF(["essayId","[reviewerId, workTime]","avgTime","STD"])
#display(e)
#----------------------------------------------------------end of calculation of avg, std, ztime-------------------------


#calculates each reviewers z time column
def revZcol(row):
  tempRevlist=[]
  for i in row[4]:
    tempRevlist.append(i)
  return tempRevlist

#cast to string for sorting by reviewer's id
def numToStr(row):
  for i in row:
    i[0]=str(i[0])
  return row

#list of reviewer ID's for later dictionary use
listOfRevs=temp.map(lambda x: (x[0])).distinct().map(lambda x: [int(x)]).sortBy(lambda a: a).map(lambda x: str(x[0])).collect()#.toDF()#


#essRdd is [essayId, [revId,zTime]]
essRdd=e.map(lambda x: revZcol(x)).map(lambda x: numToStr(x))#.collect()#.toDF()##.toDF()
essList=essRdd.collect()
#l1=list of lists of [rev,z], l2= list of rev

#create empty dictionary
mydict= {}

#fill dictionary with zeroes, stands for reviewers that had not checked certain essays
for i in listOfRevs:
  mydict[i]= [float(0) for i in range(500)]

#fill dictionary with actual z time for essays that were checked by reviewers
tempCounter=0
for i in essList:
  for j in i:
    temp=j[0]
    mydict[temp][tempCounter]=j[1]
  tempCounter+=1
  
#create empty list to be filled with the dictionary data
dictList=[]  

#fill data from dictionary of type (revID, Ztime or 0 if not checked)
for i in mydict:
  dictList.append([i,mydict[i]])

#create rdd from list
dfMatrix=sc.parallelize(dictList)#.collect()
jjj=dfMatrix.toDF()

#-------------------------------------------------------------------Kmeans clustering------------------------------
#dense vec accumulates data from array to type that k means can accept
def denseVector(row):
  return row+[Vectors.dense(row[1])]

#----1. calculation with zeroes and aranged by essays order--------------

#apply dense vec function on matrix and turn to Data Frame
p=dfMatrix.map(lambda row: denseVector(row)).toDF(["reviewerID", "ZtimeArray", "timeVector"])

#sort by reviewers' ID column
sortedByRev1=p.sort(['reviewerID'], ascending=True)

#apply vector assembler on timeVector column, output column= features
#the name has to be features or else the built-in algorithm won't acknowledge it 
vecAssembler1 = VectorAssembler(inputCols=["timeVector"], outputCol="features")

#join the 2 DF's 
new_df1 = vecAssembler1.transform(sortedByRev1)

#apply the k-means algorith
kmeans1 = KMeans(k=4, seed=2)

#the model is created and is responsible for the centroids calculation 
model1 = kmeans1.fit(new_df1.select("features")) 

#new DF created containing the clustering prediction column
transformed1 = model1.transform(new_df1)

#delete unnecessary colums
KmeansDF1=transformed1.drop('features', 'timeVector')

# centroids from k-means calculation
centers = model1.clusterCenters()

#convert list to rdd
centersRdd=sc.parallelize(centers)

#convert numpy array to list for convertion to DF
centersDf=centersRdd.map(lambda x: x.tolist()).toDF()#.collect()
#---------------------------------End Of Calculation 1----------------------

#----2. calculation without zeroes, sorted by each reviewer's zTime (in ascending order)--------------
#dense vec accumulates data from array to type that k means can accept
def denseVector1(row):
  return [row[0],row[1],Vectors.dense(row[1])]

#remove zeroes from the first calculation's rdd
def removeZeroes(row):
  for num in row[1]:
    if num==float(0):
      row[1].remove(num)
  return [row[0], row[1]]

#apply remove zero function
revZtimeRdd=dfMatrix.map(lambda x: removeZeroes(x))#.map(lambda x: [x[0],list(y for y in x[1] if y!=0)]).map(lambda x: [x[0],sorted(x[1])])

#apply dense vector function
densedvec=revZtimeRdd.map(lambda row: denseVector1(row)).toDF(["reviewerID", "timeArray", "timeVec"])

#sort DF by reviewers' ID 
sortedByRev2=densedvec.sort(['reviewerID'], ascending=True)

#apply vector assembler on timeVector column, output column= features
#the name has to be features or else the built-in algorithm won't acknowledge it
vecAssembler2 = VectorAssembler(inputCols=["timeVec"], outputCol="features")

#join the 2 DF's 
new_df2 = vecAssembler2.transform(sortedByRev2)

#apply the k-means algorith
kmeans2 = KMeans(k=3, seed=2)

#the model is created and is responsible for the centroids calculation
model2 = kmeans2.fit(new_df2.select("features"))

#new DF created containing the clustering prediction column
transformed2 = model2.transform(new_df2)

#delete unnecessary colums
KmeansDF2=transformed2.drop('features', 'timeVector')

#---------------------------------End Of Calculation 2----------------------

#-------------------------------------------------------------------End Of Kmeans clustering-----------------------

display(df1)
